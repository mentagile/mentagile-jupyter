# Learn Better By Staying Cool (part 3, jupyter) #

Part of the hackster.io project:
Learn Better By Staying Cool
https://www.hackster.io/mentagile/learn-better-by-staying-cool-2618ed

The Arduino project captures PPG heart rate data while playing a game of snake and learns to classify 
and predict different stress states experienced during the game.

### What is this repository for? ###

* The jupyter notebook for processing and training the data https://colab.research.google.com
* https://jupyter.org
* https://colab.research.google.com

### How do I get set up? ###

* The jupyter notebook for creating the ppg training data (mostly) automatically from the snake game and the Smart Fun sensors is in this repository in Downloads.
* The labeled data is stored in an sqlite database created by the software found on the hackster.io project page
* The sqlite.db file can be linked through your Google drive or uploaded separately with either Google Colab or your own Jupyter installation.
* You can open the notebook in colab or in your own Jupyter notebook

### Model performance

* The model layers as set in the notebook generate parameters that consistently get 95% or higher accuracy predicting stress/non-stress


### Who do I talk to? ###

* See the Hackster.io project page is the place to start if you are looking for more information or if you have any questions.